Name:           SDL3
Version:        3.1.3
Release:        1%{?dist}
Summary:        Cross-platform multimedia library
License:        Zlib AND MIT AND Apache-2.0 AND (Apache-2.0 OR MIT)
URL:            https://www.libsdl.org/
Source0:        https://github.com/libsdl-org/SDL/releases/download/preview-3.1.3/SDL3-3.1.3.tar.xz
# Source0:        https://www.libsdl.org/release/%{name}-%{version}.tar.xz

BuildRequires:  git-core
BuildRequires:  cmake
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  alsa-lib-devel
# BuildRequires:  mesa-libGL-devel
BuildRequires:  mesa-libGLU-devel
BuildRequires:  mesa-libEGL-devel
BuildRequires:  mesa-libGLES-devel
#BuildRequires:  libXext-devel
#BuildRequires:  libX11-devel
#BuildRequires:  libXi-devel
#BuildRequires:  libXrandr-devel
#BuildRequires:  libXrender-devel
#BuildRequires:  libXScrnSaver-devel
#BuildRequires:  libXinerama-devel
#BuildRequires:  libXcursor-devel
BuildRequires:  systemd-devel
#BuildRequires:  pkgconfig(libusb-1.0)
# PulseAudio
BuildRequires:  pkgconfig(libpulse-simple)
# Jack
BuildRequires:  pkgconfig(jack)
# PipeWire
BuildRequires:  pkgconfig(libpipewire-0.3)
# D-Bus
BuildRequires:  pkgconfig(dbus-1)
# IBus
# BuildRequires:  pkgconfig(ibus-1.0)
# Wayland
#BuildRequires:  pkgconfig(libdecor-%{libdecor_majver})
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-cursor)
#BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-scanner)
BuildRequires:  pkgconfig(xkbcommon)
# Vulkan
BuildRequires:  vulkan-devel
# KMS
# BuildRequires:  mesa-libgbm-devel
# BuildRequires:  libdrm-devel

# Ensure libdecor is pulled in when libwayland-client is (rhbz#1992804)
# Requires:       (libdecor-%{libdecor_majver}.so.%{libdecor_majver}%{libsymbolsuffix} if libwayland-client)

%description
Simple DirectMedia Layer (SDL) is a cross-platform multimedia library designed
to provide fast access to the graphics frame buffer and audio device.

%package devel
Summary:        Files needed to develop Simple DirectMedia Layer applications
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       mesa-libEGL-devel%{?_isa}
Requires:       mesa-libGLES-devel%{?_isa}
Requires:       libX11-devel%{?_isa}

%description devel
Simple DirectMedia Layer (SDL) is a cross-platform multimedia library designed
to provide fast access to the graphics frame buffer and audio device. This
package provides the libraries, include files, and other resources needed for
developing SDL applications.

%package static
Summary:        Static libraries for SDL3
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}

%description static
Static libraries for SDL3

%prep
%autosetup


# -DSDL_PIPEWIRE=ON \
# -DSDL_ALSA=ON \
# -DSDL_PULSEAUDIO=ON \
# -DSDL_JACK=ON \
# -DSDL_WAYLAND=ON \
%build
%cmake \
    -DSDL_ALSA=ON \
    -DSDL_ALSA_SHARED=OFF \
    -DSDL_PIPEWIRE=OFF \
    -DSDL_PIPEWIRE_SHARED=OFF \
    -DSDL_PULSEAUDIO=ON \
    -DSDL_PULSEAUDIO_SHARED=OFF \
    -DSDL_JACK=OFF \
    -DSDL_JACK_SHARED=OFF \
    -DSDL_SSE3=OFF \
    -DSDL_SSE4_1=OFF \
    -DSDL_SSE4_2=OFF \
    -DSDL_AVX=OFF \
    -DSDL_AVX2=OFF \
    -DSDL_AVX512F=OFF \
    -DSDL_RPATH=ON \
    -DSDL_STATIC=ON \
    -DSDL_STATIC_PIC=ON \
    -DSDL_X11=OFF \
    -DSDL_X11_SHARED=OFF \
    -DSDL_WAYLAND=ON \
    -DSDL_WAYLAND_SHARED=OFF \
    -DSDL_VULKAN=ON \
    -DSDL_RENDER_VULKAN=ON \
    -DSDL_LIBUDEV=ON \
    -DSDL_OPENGL=ON \
    -DSDL_OPENGLES=ON
%cmake_build

%install
%cmake_install
rm -rf %{buildroot}/usr/share

%files
%license LICENSE.txt
%doc BUGS.txt CREDITS.md README-SDL.txt
%{_libdir}/libSDL3.so*

%files devel
%doc README.md WhatsNew.txt INSTALL.md
#%{_bindir}/*-config
%{_libdir}/libSDL3.so*
%{_libdir}/libSDL3.a
%{_libdir}/pkgconfig/sdl3.pc
%dir %{_libdir}/cmake/SDL3
%{_libdir}/cmake/SDL3/SDL3Config*.cmake
%{_libdir}/cmake/SDL3/SDL3sharedTargets*.cmake
%{_libdir}/cmake/SDL3/SDL3headersTargets*.cmake
%{_libdir}/cmake/SDL3/SDL3staticTargets*.cmake
%{_includedir}/SDL3
#%{_datadir}/aclocal/*
%{_libdir}/libSDL3_test.a
%{_libdir}/cmake/SDL3/SDL3testTargets*.cmake

%files static
%license LICENSE.txt
%{_libdir}/libSDL3.a
%{_libdir}/cmake/SDL3/SDL3staticTargets*.cmake

%changelog
